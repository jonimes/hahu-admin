import { defineRule } from "vee-validate";
import i18n from "@/plugins/i18n";

defineRule("required", (value) => {
  if (typeof value === "number") {
    return true;
  }
  return !!value || i18n.global.t("this_field_is_required");
});

defineRule("number", (value) => {
  return (
    !value ||
    typeof value === "number" ||
    i18n.global.t("this field must be number")
  );
});


defineRule("email", (value) => {
  return (
    !value ||
    /[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
      value
    ) ||
    i18n.global.t("invalid_email")
  );
});

defineRule("password", (value) => {
  return !value || value.length >= 8 || i18n.global.t("password_too_short");
});
defineRule("confirm", (value, [other]) => {
  return !value || value === other || i18n.global.t("passwords_do_not_match");
});

defineRule("International_phone_number", (value) => {
  return (
    !value || /^\+(?:[0-9] ?){6,14}[0-9]$/.test(value) || i18n.global.t("invalid_phone")
  )
});

