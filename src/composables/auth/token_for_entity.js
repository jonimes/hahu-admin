import token_for_entity_gql from "@/queries/auth/token_for_entity.gql";
import { useQuery } from "@vue/apollo-composable";

import useNotify from "@/use/notify";
import useErrorParser from "@/use/errorParser";

const { notify } = useNotify();
const { parse } = useErrorParser();

export default function (entity, enabled) {
  const { onError, onResult, loading } = useQuery(
    token_for_entity_gql,
    () => ({
      entity_id: entity.value.entity_id,
    }),
    () => ({
      enabled: enabled.value,
      fetchPolicy: "no-cache",
    })
  );

  onError((err) => notify(parse(err)));

  return {
    onResult,
    loading,
  };
}
