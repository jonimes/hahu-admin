import { useQuery } from "@vue/apollo-composable";
import basic_entities from "@/queries/auth/basic_entities.gql";

import useNotify from "@/use/notify";
import useErrorParser from "@/use/errorParser";

const { notify } = useNotify();
const { parse } = useErrorParser();

export default function () {
  const { onResult, loading, onError } = useQuery(basic_entities, () => ({
    fetchPolicy: "no-cache",
    clientId: "entity",
    context: {
      headers: {
        "x-hasura-role": "users_management",
      },
    },
  }));

  onError((err) => notify(parse(err)));

  return {
    onResult,
    loading,
  };
}
