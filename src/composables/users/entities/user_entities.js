import entities from "@/queries/user/entities/user_entities.gql";
import { useQuery } from "@vue/apollo-composable";

export default (offset, limit, where) => {
  const {onResult, onError, loading, refetch} = useQuery(entities, () => ({
    offset: offset.value,
    limit: limit.value,
    where: where.value,
  }), () => ({
      fetchPolicy: "no-cache",
      clientId: 'entity',
      context: {
        headers: {
          "x-hasura-role": "users_management",
        },
      },
  }));

  return{
      onResult,
      onError,
      loading,
      refetch
  }
};
