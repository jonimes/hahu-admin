import entity_users from "@/queries/user/entities/entity_users.gql";
import { useQuery } from "@vue/apollo-composable";

export default (offset, limit, id) => {
  const { onResult, onError, loading, refetch } = useQuery(
    entity_users,
    () => ({
      offset: offset.value,
      limit: limit.value,
      id: id,
    }),
    () => ({
      fetchPolicy: "no-cache",
      clientId: "entity",
      context: {
        headers: {
          "x-hasura-role": "users_management",
        },
      },
    })
  );

  return {
    onResult,
    onError,
    loading,
    refetch,
  };
};
