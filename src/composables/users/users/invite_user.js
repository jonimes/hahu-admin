import invite_user from "@/queries/user/users/invite.gql"
import { useMutation } from "@vue/apollo-composable";

export default () => {
  const { mutate, onDone, onError, loading } = useMutation(invite_user, () => ({
    fetchPolicy: "no-cache",
    clientId: "entity",
    context: {
      headers: {
        "x-hasura-role": "users_management",
      },
    },
  }));

  return {
      mutate,
      onDone,
      onError,
      loading
  };
};
