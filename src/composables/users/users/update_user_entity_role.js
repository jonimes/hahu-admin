import update_user_entity_roles from "@/queries/user/users/update_user_entity_roles.gql"
import { useMutation } from "@vue/apollo-composable";


export default () => {
    const { mutate, onDone, onError } = useMutation(update_user_entity_roles, () => ({
      fetchPolicy: "no-cache",
      clientId: "entity",
      context: {
        headers: {
          "x-hasura-role": "users_management",
        },
      },
    }));
  
    return {
      mutate,
      onDone,
      onError,
    };
  };