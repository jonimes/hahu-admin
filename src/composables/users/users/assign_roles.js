import assign_user_entity_roles from "@/queries/user/users/insert-user-entity-roles.gql"
import { useMutation } from "@vue/apollo-composable";
export default () => {
  const { mutate, onDone, onError, loading } = useMutation(
    assign_user_entity_roles,
    () => ({
      fetchPolicy: "no-cache",
      clientId: "entity",
      context: {
        headers: {
          "x-hasura-role": "users_management",
        },
      },
    })
  );

  return {
    mutate,
    onDone,
    onError,
    loading,
  };
};