import change_password from "@/queries/user/users/change_password.gql";
import { useMutation } from "@vue/apollo-composable";

export default () => {
  const { mutate, onDone, loading, onError } = useMutation(
    change_password,
    () => ({
      fetchPolicy: "no-cache",
      clientId: "entity",
      context: {
        headers: {
          "x-hasura-role": "users_management",
        },
      },
    })
  );

  return {
      mutate,
      onDone, 
      loading,
      onError
  }
};
