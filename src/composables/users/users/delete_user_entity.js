import delete_user_entity from "@/queries/user/users/delete_user_entity.gql"
import { useMutation } from "@vue/apollo-composable";
export default () => {
  const { mutate, onDone, onError } = useMutation(delete_user_entity, () => ({
    fetchPolicy: "no-cache",
    clientId: "entity",
    context: {
      headers: {
        "x-hasura-role": "users_management",
      },
    },
  }));

  return {
    mutate,
    onDone,
    onError,
  };
};