import user_gql from "@/queries/user/users/user.gql";
import { useQuery } from "@vue/apollo-composable";

export default (id) => {
  const { onError, onResult, loading, refetch } = useQuery(
    user_gql,
    { id: id },
    () => ({
      fetchPolicy: "no-cache",
      clientId: "entity",
      context: {
        headers: {
          "x-hasura-role": "users_management",
        },
      },
    })
  );

  return{
      onResult,
      onError,
      loading,
      refetch
  }
};
