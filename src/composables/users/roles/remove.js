import { useMutation } from "@vue/apollo-composable";
import remove from "@/queries/user/roles/remove.gql";

export default () => {
  const {mutate, onDone, onError} = useMutation(remove, () => ({
    fetchPolicy: "no-cache",
    clientId: "entity",
    context: {
      headers: {
        "x-hasura-role": "users_management",
      },
    },
  }));

  return{
      mutate,
      onDone,
      onError
  }
};
