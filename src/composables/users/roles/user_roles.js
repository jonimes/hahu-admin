import user_roles from "@/queries/user/roles/user_roles.gql";
import { useQuery } from "@vue/apollo-composable";

export default (id, enabled) => {
  const { onResult, onError, loading, refetch } = useQuery(
    user_roles,
    () => ({
      id: id.value,
    }),
    () => ({
      enabled: enabled.value,
      fetchPolicy: "no-cache",
      clientId: "entity",
      context: {
        headers: {
          "x-hasura-role": "users_management",
        },
      },
    })
  );

  return {
    onResult,
    onError,
    loading,
    refetch,
  };
};
