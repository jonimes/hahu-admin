import role from "@/queries/user/roles/role.gql";
import { useQuery } from "@vue/apollo-composable";

export default (name) => {
  const { onResult, onError, loading, refetch } = useQuery(
    role,
    { name: name },
    () => ({
      fetchPolicy: "no-cache",
      clientId: "entity",
      context: {
        headers: {
          "x-hasura-role": "users_management",
        },
      },
    })
  );

  return {
    onResult,
    onError,
    loading,
    refetch,
  };
};
