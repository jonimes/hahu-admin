import save from "@/queries/user/roles/save.gql";
import { useMutation } from "@vue/apollo-composable";

export default () => {
  const {mutate, onDone, onError, loading} = useMutation(save, () => ({
    fetchPolicy: "no-cache",
    clientId: "entity",
    context: {
      headers: {
        "x-hasura-role": "users_management",
      },
    },
  }));

  return {
      mutate,
      onDone,
      onError,
      loading
  }
};
