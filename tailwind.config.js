const path = require("path");
module.exports = {
  darkMode: 'class',
  mode: "jit",
  purge: [
    "./index.html",
    "./src/**/*.{vue,js}",
    path.resolve(__dirname, "./node_modules/litepie-datepicker/**/*.js"),
  ],
  theme: {
    screens: {
      xs: "512px",
      // => @media (min-width: 512px) { ... }

      sm: "640px",
      // => @media (min-width: 640px) { ... }

      md: "768px",
      // => @media (min-width: 768px) { ... }

      lg: "1024px",
      // => @media (min-width: 1024px) { ... }

      xl: "1280px",
      // => @media (min-width: 1280px) { ... }

      "2xl": "1536px",
      // => @media (min-width: 1536px) { ... }

      "3xl": "1600px",
      // => @media (min-width: 1600px) { ... }
      "4xl": "1920px",
      // => @media (min-width: 1920px) { ... }
    },
    fontFamily: {
      body: ["Manrope"],
      diplay: ["Manrope"],
      sans: ["Manrope"],
      serif: ["Manrope"],
    },

    extend: {
      backgroundImage: {},
      boxShadow: {
        full: "0 3px 1px -2px rgba(0,0,0, 0.2), 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12) ",
      },
      lineClamp: {
        6: "6",
        7: "7",
        8: "8",
        9: "9",
        10: "10",
      },
      fontSize: {
        md: ["1.125rem", "1.5rem"],
      },
      maxWidth: {
        six: "102rem",
      },
      height: {
        nav: "5.25rem",
        dscreen: "calc(100vh - 5rem)",
      },

      flex: {
        six: "1 0 16.6%",
      },
      width: {
        bar: "220px",
        100: "26rem",
        102: "30rem",
        dscreen: "calc(100vw - 47.5rem)",
      },
      padding: { navP: "220px" },
      colors: {
        HahuGray1: "#444F60",
        HahuGray2: "#697280",
        HahuGray3: "#C7CACF",
        HahuGray4: "#C4C4C4",
        "HahuGray/4": "#ECEDEF",
        HahuGray5: "#596474",
        "HaHuGreen/2": "#80CBC4",
        "Hahugreen/3": "#B3E0DB",
        "HaHuGreen/4": "#E6F5F3",
        HahuRed: "#FF007A",
        DarkModeBg: "#263142",
        primary: "#009688",
        "primary-lite": "#6ec8c0;",
        "primary-dark": "#007166",
        secondary: "#444f60",
        "secondary-4": "#E5E5E5",
        "secondary-dark": "#313131",


        primary: "#009688",
        "primary-lite": "#6ec8c0;",
        "primary-dark": "#444f60",
        "HaHuGreen/2": "#80CBC4",
        "Hahugreen/3": "#B3E0DB",
        "HaHuGreen/4": "#E6F5F3",
        secondary: "#444f60",
        HahuGray1: "#444F60",
        HahuGray2: "#697280",
        HahuGray3: "#C7CACF",
        HahuGray4: "#C4C4C4",
        "HahuGray/4": "#ECEDEF",
        HahuGray5: "#596474",
        HahuRed: "#FF007A",
        DarkModeBg: "#263142",
        "secondary-1": "#ECECEC",
        "secondary-2": "#868686",
        "secondary-3": "#F8F8F8",
        "secondary-4": "#F9FAFB",
        "secondary-dark": "#0F172A",
        'secondary-light': '#FFFFFF',
        whitePrimary: "#F0FDF4",
        "secondary-nav": "#F9FAFB",
        "tertiary-dark": "#1E293B",
        "footer-text": "#888888",
        "litepie-primary": {
          50: "#009688",
          100: "#009688",
          200: "#009688",
          300: "#009688",
          400: "#009688",
          500: "#009688",
          600: "#009688",
          700: "#009688",
          800: "#009688",
        }, // color system for light mode
        "litepie-secondary": {
          50: "#444F60",
          100: "#009688",
          200: "#444F60",
          300: "#565656",
          400: "#444F60",
          500: "#444F60",
          600: "#444F60",
          700: "#444F60",
          800: "#444F60",
        },
      },
    },
  },
  variants: {
    extend: {
      backgroundOpacity: ["disabled"],
      cursor: ["disabled"],
      borderWidth: ["last", "focus-within", "first"],
      borderRadius: ["hover"],
    },
  },
  plugins: [require("@tailwindcss/forms"),
  require('@tailwindcss/line-clamp')
  ],
};
